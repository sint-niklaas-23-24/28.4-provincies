﻿namespace _28._4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Gemeente Kasterlee = new Gemeente("2460", "Kasterlee");
            Gemeente Geel = new Gemeente("2440", "Geel");
            Gemeente Brussel = new Gemeente("1000", "Brussel");
            Gemeente Lille = new Gemeente("2275", "Lille");
            Provincie antwerpen = new Provincie("Antwerpen");
            antwerpen.AddGemeente(Kasterlee);
            antwerpen.AddGemeente(Geel);
            antwerpen.AddGemeente(Brussel);
            antwerpen.AddGemeente(Lille);
            Console.WriteLine(antwerpen.ToString());
            Console.ReadLine();
            Console.WriteLine("Na het verwijderen van Brussel");
            antwerpen.RemoveGemeente(Brussel);
            Console.WriteLine(antwerpen.ToString());






        }
    }
}
