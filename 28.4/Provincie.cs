﻿namespace _28._4
{
    internal class Provincie
    {
        private string _provincienaam;
        private List<Gemeente> _gemeentes;
        public Provincie(string provincie)
        {
            Provincienaam = provincie;
            Gemeentes = new List<Gemeente>();
        }
        private List<Gemeente> Gemeentes { get { return _gemeentes; } set { _gemeentes = value; } }
        public string Provincienaam { get { return _provincienaam; } set { _provincienaam = value; } }
        public void AddGemeente(Gemeente gemeente)
        {
            bool gemeenteGelijk = false;
            foreach (Gemeente gemeenteItem in Gemeentes)
            {
                if (gemeenteItem.Equals(gemeente))
                {
                    Console.WriteLine("Gemeente bestaat al! U kan deze niet opnieuw toevoegen");
                    gemeenteGelijk = true;
                }
            }
            if (!gemeenteGelijk)
            {
                Gemeentes.Add(gemeente);
            }

        }
        public void RemoveGemeente(Gemeente gemeente)
        {
            foreach (Gemeente gemeenteItem in Gemeentes)
            {
                if (gemeente == gemeenteItem)
                {
                    Gemeentes.Remove(gemeenteItem);
                    break;
                }
            }
        }
        public override string ToString()
        {
            string display;
            display = "Provincie " + Provincienaam + Environment.NewLine;
            foreach (Gemeente gemeente in Gemeentes)
            {
                display += gemeente.ToString() + Environment.NewLine; //Checken sebiet of newline nodig is
            }
            return display;
        }
    }
}
