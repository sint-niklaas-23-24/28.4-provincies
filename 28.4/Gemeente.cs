﻿namespace _28._4
{
    internal class Gemeente
    {
        private string _gemeenteNaam;
        private string _postcode;
        public Gemeente(string postcode, string gemeente)
        {
            Postcode = postcode;
            GemeenteNaam = gemeente;
        }
        public string GemeenteNaam { get { return _gemeenteNaam; } set { _gemeenteNaam = value; } }
        public string Postcode { get { return _postcode; } set { _postcode = value; } }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Gemeente g = (Gemeente)obj;
                    if (this.GemeenteNaam == g.GemeenteNaam && this.Postcode == g.Postcode)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return Postcode + " - " + GemeenteNaam;
        }
    }
}
